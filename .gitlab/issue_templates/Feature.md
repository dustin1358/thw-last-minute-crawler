<!---
Stelle sicher, dass es sich nicht um ein Duplikat handelt!
--->

### Zu lösendes Problem

<!--- Welches Problem soll gelöst werden? --->

### Weitere Details

<!--- Füge Use cases, Nutzen und/oder Ziele des Features hinzu --->

### Vorschläge

<!--- Wie sollte das Problem gelöst werden? --->

### Wie sieht die Erfolgreiche Implementation aus?

<!--- Was muss erfüllt sein, damit das Feature erfolgreich implementiert ist? --->

### Links / Anhänge

<!--- Relevante Links? --->

/label ~Feature

