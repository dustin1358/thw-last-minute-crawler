<!---
Stelle sicher, dass es sich nicht um ein Duplikat handelt!
--->

### Zusammenfassung

<!--- Fasse den Bug möglichst exakt zusammen --->

### Wie kann der Bug reproduziert werden?

<!--- Genaue Schritte, um den Bug zu reproduzieren --->

### Beispiel branch/commit

<!--- Auf welchem branch oder besser bei welchem commit tritt der Bug auf? --->

### Wie verhält sich der Bug?

<!--- Was passiert? --->

### Was sollte korrekterweise passieren?

<!--- Was im Idealfall angezeigt werden sollte --->

### Anhang relevanter Screenshots/Logs

<!--- Füge hier alle relevanten Logs ein - bitte benutze code blocks (```) um terminal output, logs,
und code zu formattieren --->

### Mögliche Ursachen/Fixes

<!--- Verlinke wenn möglich die Stelle im Code, die für den Bug verantwortlich sein könnte --->

/label ~Bug

