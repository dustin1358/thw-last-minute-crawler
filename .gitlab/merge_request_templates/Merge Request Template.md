## Was tut dieser MR?

<!-- Beschreibe kurz den Gegenstand dieses MR. -->

## Relevante Tickets

Closes <!-- Hier das/die Ticket(s) einfügen welche(s) geschlossen werden soll. -->

<!-- Hier auf für diesen MR relevanten Tickets verweisen die nicht geschlossen werden sollen. -->

## Developer checklist

- [ ] Ersten beiden Abchnitte ausgefüllt.
- [ ] Code ist dokumentiert.
- [ ] WIP-Status aufgelöst ("Resolve WIP status" button benutzen oder `/wip` als Kommentar hinzufügen).
- [ ] Assignee ticket to 'Dustin Matzel'.

**Beachte:** MR bitte angemessen früh vor dem Meeting erstellen! Der Reviewer muss genug Zeit zum bearbeiten des MR und für Feedback haben.

## Reviewer checklist

- [ ] Das Ticket wird geschlossen, wenn der MR geschlossen wird.
- [ ] Der Code löst das Problem.
- [ ] Der Code ist kommentiert.
- [ ] CI failed nicht.
- [ ] Keine Merge-Konflikte.


