/*
 * Copyright (C) 2017-2020 Dustin Matzel.
 *
 * Developers:
 * Dustin Matzel
 *
 * In case that the code will be changed or modified, the developers'
 * names MUST be kept in the license.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package matzel.dustin.thwlastminutecrawler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Adapter which fills the RecyclerView entries.
 */
class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

    /**
     * The names of the courses
     */
    private String[] names;

    /**
     * The free places of the courses.
     */
    private String[] freePlaces;

    /**
     * ViewHolder which holds the relativeLayout.
     */
    static class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout mRelativeLayout;
        ViewHolder(RelativeLayout v) {
            super(v);
            mRelativeLayout = v;
        }
    }

    /**
     * Constructor for the Adapter.
     * @param names String array of the courses.
     * @param freePlaces String array of the free places.
     */
    MyAdapter(String[] names, String[] freePlaces) {
        this.names = names;
        this.freePlaces = freePlaces;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder((RelativeLayout)v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        TextView title = holder.mRelativeLayout.findViewById(R.id.text);
        title.setText(names[position]);

        TextView content = holder.mRelativeLayout.findViewById(R.id.freePlaces);
        if(freePlaces.length>position) {
            String contentText = freePlaces[position];
            content.setText(contentText);
        }

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return names.length;
    }

}