/*
 * Copyright (C) 2017-2020 Dustin Matzel.
 *
 * Developers:
 * Dustin Matzel
 *
 * In case that the code will be changed or modified, the developers'
 * names MUST be kept in the license.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package matzel.dustin.thwlastminutecrawler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;

import static android.content.Context.ALARM_SERVICE;

/**
 * Utility contains static methods which are used at multiple places.
 */
abstract class Utility {

	/**
	 * URL auf the last minute website.
	 */
	static String THW_LAST_MINUTE_URL = "https://www.thw-bundesschule.de/THW-BuS/DE/Ausbildungsangebot/Lehrgangskalender/lehrgangskalender_node.html?sort=lastMinute";

	/**
	 * The key under which the courses are saved in the SharedPreferences.
	 * Do not aks me why I saved them under this key:D
	 */
	static String COURSES_ACCESS_KEY = "kartoffel";

	/**
	 * Default text if nothing is saved in the SharedPreferences for the courses.
	 */
	static String COURSES_DEFAULT_TEXT = "Beim nächsten Start stehen hier die aktuellen Last-Minute Lehrgänge!";

	/**
	 * They key under which the amount of free places are saved.
	 */
	static String COURSES_AMOUNT_ACCESS_KEY = "plaetze";

	/**
	 * TODO: Not surer for what this is used.
	 */
	static String UNKNOWN_ACCESS_KEY = "puerre";

	/**
	 * Sets an alarm that the NotificationBroadcastReciever get called every hour.
	 * @param context The given context.
	 */
	static void setAlarm(Context context){
		Intent mIntent= new Intent(context, NotificationBroadcastReceiver.class);
		AlarmManager malaramManager= (AlarmManager) context.getSystemService(ALARM_SERVICE);
		PendingIntent pendingIntent= PendingIntent.getBroadcast(context, 0, mIntent, 0);
		malaramManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime(), AlarmManager.INTERVAL_HOUR, pendingIntent);
	}

	/**
	 * Checks if the device has an internet connection.
	 * @param context The given context.
	 * @return True if it is connected with the internet, false otherwise.
	 */
	static boolean isConnectedToInternet(Context context){
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null){
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (NetworkInfo anInfo : info) {
					if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
		}
		return false;
	}


}
