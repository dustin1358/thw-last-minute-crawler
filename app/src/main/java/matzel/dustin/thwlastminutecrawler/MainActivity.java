/*
 * Copyright (C) 2017-2020 Dustin Matzel.
 *
 * Developers:
 * Dustin Matzel
 *
 * In case that the code will be changed or modified, the developers'
 * names MUST be kept in the license.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package matzel.dustin.thwlastminutecrawler;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import static matzel.dustin.thwlastminutecrawler.Utility.COURSES_AMOUNT_ACCESS_KEY;
import static matzel.dustin.thwlastminutecrawler.Utility.COURSES_DEFAULT_TEXT;
import static matzel.dustin.thwlastminutecrawler.Utility.COURSES_ACCESS_KEY;
import static matzel.dustin.thwlastminutecrawler.Utility.THW_LAST_MINUTE_URL;

/**
 * MainActivity is the starting point of the app.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * RecyclerView which contains the courses.
     */
    RecyclerView lehrgangsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);

        String lehrgaenge = sharedPreferences.getString(COURSES_ACCESS_KEY, COURSES_DEFAULT_TEXT);
        String anzahl = sharedPreferences.getString(COURSES_AMOUNT_ACCESS_KEY, " ");


        String[] ary = lehrgaenge.split("\n");
        String[] aryAnz = anzahl.split("\n");


        lehrgangsList = findViewById(R.id.listView);

        RecyclerView.Adapter mAdapter;
        RecyclerView.LayoutManager mLayoutManager;

        lehrgangsList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        lehrgangsList.setHasFixedSize(true);


        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        lehrgangsList.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(ary, aryAnz);
        lehrgangsList.setAdapter(mAdapter);


        Utility.setAlarm(this);

    }

    int senseless = 1;

    public void senseless(View view){
        if(senseless ==1){
            ((Button)view).setText("Dieser Button hat keine Funktion!");
        } else if(senseless ==2) {
            ((Button) view).setText("Dieser Button hat immer noch keine Funktion!");
        }else if(senseless ==42){
            ((Button) view).setText("42");
        } else{
            ((Button)view).setText("Dieser Button hat auch beim "+ senseless +". Mal keine Funktion!");
        }
        senseless++;
    }


    MenuItem mi;
    @Override
    public boolean onCreateOptionsMenu(Menu mMenu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, mMenu);
        mi = mMenu.findItem(R.id.locate);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Checks if menu element with ID "locate"
        // was clicked and open then the website
        int id = item.getItemId();
        if (id == R.id.locate) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(THW_LAST_MINUTE_URL));
            startActivity(i);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

