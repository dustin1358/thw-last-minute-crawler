/*
 * Copyright (C) 2017-2020 Dustin Matzel.
 *
 * Developers:
 * Dustin Matzel
 *
 * In case that the code will be changed or modified, the developers'
 * names MUST be kept in the license.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package matzel.dustin.thwlastminutecrawler;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static matzel.dustin.thwlastminutecrawler.Utility.COURSES_ACCESS_KEY;
import static matzel.dustin.thwlastminutecrawler.Utility.COURSES_AMOUNT_ACCESS_KEY;
import static matzel.dustin.thwlastminutecrawler.Utility.UNKNOWN_ACCESS_KEY;
import static matzel.dustin.thwlastminutecrawler.Utility.THW_LAST_MINUTE_URL;


/**
 * This Services downloads the website, parses it and display the notification.
 */
public class NotificationService extends Service {


    SharedPreferences preferences;
    SharedPreferences.Editor se;
    String coursesServer;
    int amountOfCourses;
    Context context;


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context=this;
        if(intent!=null){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(Utility.isConnectedToInternet(NotificationService.this)) {
                        preferences = PreferenceManager.getDefaultSharedPreferences(context);
                        se = preferences.edit();
                        coursesServer = getConvertedString(getDivString());
                        String app = PreferenceManager.getDefaultSharedPreferences(context).getString(COURSES_ACCESS_KEY, "");
                        if (!coursesServer.equals(app)) {
                            if(!app.equals("")&&!coursesServer.equals("")) {
                                showNotification();
                                se.putString(UNKNOWN_ACCESS_KEY, app);
                                se.apply();
                            }
                            if(!coursesServer.equals("")) {
                                se.putString(COURSES_ACCESS_KEY, coursesServer);
                                se.apply();
                                stopThis();
                            }
                        }
                    }
                }
            }).start();
        }else{
            stopThis();
            Log.i("SERVICE-TAG", "service stopt itself");
        }
        return START_NOT_STICKY;
    }

    private void stopThis(){
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
            String CHANNEL_ID = "matzel_dustin_thwlastminutecrawler_none";
            String CHANNEL_NAME = "None";

            NotificationChannel notificationChannel;
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_MIN);
            notificationChannel.enableLights(false);
            notificationChannel.enableVibration(false);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(notificationChannel);

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this, CHANNEL_ID)
                            .setContentTitle("")
                            .setContentText("");
            startForeground(1,mBuilder.build());
            stopSelf();
        }
    }

    private String getConvertedString(String[] servAry) {
        amountOfCourses =servAry.length;
        String serv ="";
        for(int i=0;i<servAry.length;i++){
            if(i==0){
                serv=servAry[i];
            }else {
                serv = serv + "\n" + servAry[i];
            }
        }
        return serv;
    }


    private String[] getDivString(){
        String s="";
        ArrayList<String> as = new ArrayList<>();
        ArrayList<String> freePlaces = new ArrayList<>();
        try {
            DefaultHttpClient client = new DefaultHttpClient();
            URL url = new URL(THW_LAST_MINUTE_URL);
            HttpGet get = new HttpGet(url.toURI());
            HttpResponse resp = client.execute(get);

            String content = EntityUtils.toString(resp.getEntity());
            Document doc = Jsoup.parse(content);
            Elements metalinks = doc.select("div.teaser.course");
            for (int i=0;i<metalinks.size();i++){
                Element e =metalinks.get(i);
                String txt=e.html();
                Pattern p = Pattern.compile(">(.*?)</a></h2>");
                Matcher m = p.matcher(txt);
                while (m.find()){
                    String var = m.group(1);
                    int stop = var.indexOf(">")+1;

                    if(txt.contains("Last-Minute-Plätze verfügbar")){
                        as.add(var.substring(stop));
                        int index = txt.indexOf("Last-Minute-Plätze verfügbar")-2;
                        String vars =txt.substring(index,index+1);
                        int first =Integer.parseInt(vars);
                        int second;
                        try{
                            second=Integer.parseInt(txt.substring(index-1,index));
                        }catch (Exception exp){
                            second=0;
                        }
                        vars=String.valueOf(first+10*second);
                        freePlaces.add(vars);
                    }
                }
                s=s+txt;
            }
        }catch (Exception e){
            Log.e("NotificationService","Error");
        }

        String plaetze=getConvertedString(freePlaces.toArray(new String[freePlaces.size()]));
        se.putString(COURSES_AMOUNT_ACCESS_KEY, plaetze);
        se.apply();

        return as.toArray(new String[as.size()]);
    }

    public void showNotification (){

        String CHANNEL_ID = "matzel.dustin.thwlastminutecrawler";
        String CHANNEL_NAME = "THW Lastminute Crawler";

        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setContentTitle("Die Last-Minute Seite hat sich verändert!")
                        .setAutoCancel(true)
                        .setColor(Color.BLUE)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setLights(Color.BLUE, 1000, 2000)
                        //.setLargeIcon(Bitmap.createB(R.mipmap.ic_launcher))
                        //      .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentText("Es gibt momentan "+ amountOfCourses +" Last-Minute Lehrgänge!");

        mBuilder.setSmallIcon(R.mipmap.ic_launcher);

        NotificationCompat.BigTextStyle inboxStyle =
                new NotificationCompat.BigTextStyle();
        //Sets a title for the Inbox style big view
        inboxStyle.setBigContentTitle("Diese Lehrgänge gibt es:");
        //Moves events into the big view
        inboxStyle.bigText(coursesServer);
        //Moves the big view style object into the notification object.
        mBuilder.setStyle(inboxStyle);

        //Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

        //The stack builder object will contain an artificial back stack for the
        //started Activity.
        //This ensures that navigating backward from the Activity leads out of
        //your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        //Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        //Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        //mId allows you to update the notification later on.
        //startForeground(1,mBuilder.build());
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) {
            mNotificationManager.notify(1, mBuilder.build());
        }else{
            startForeground(1,mBuilder.build());
            stopForeground(false);
        }
    }

}