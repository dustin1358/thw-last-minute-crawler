/*
 * Copyright (C) 2017-2020 Dustin Matzel.
 *
 * Developers:
 * Dustin Matzel
 *
 * In case that the code will be changed or modified, the developers'
 * names MUST be kept in the license.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package matzel.dustin.thwlastminutecrawler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * This gets repeatedly called and starts the NotificationService.
 */
public class NotificationBroadcastReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(final Context context, Intent arg1) {
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) {
			//Before android O everything worked fine, we just start a Service
			context.startService(new Intent(context, NotificationService.class));
		} else {
			//Beginning with android O we got issues with aggressive battery saving
			//See https://developer.android.com/about/versions/oreo/background.html
			context.startForegroundService(new Intent(context, NotificationService.class));
		}
	}
}
