# THW Last Minute Crawler

Project of the THW Last Minute Crawler app.
https://play.google.com/store/apps/details?id=matzel.dustin.thwlastminutecrawler

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the process for submitting merge requests to us.

## Authors

* **Dustin Matzel** - *Initial work* - [Dustin Matzel](https://gitlab.com/dustin1358)

## License

This project is licensed under Apache License Version 2.0 - see the [LICENSE](LICENSE) file for details.
