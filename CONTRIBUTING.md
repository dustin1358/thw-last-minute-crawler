# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue. 
There are two templates (one for bugs and one for features) which should be used.

## Merge Request Process

1. On the issue site is an "Create Merge Request" button. Use this one to automatically create a "work in progress" MR and a branch for the issue.
    Use the MR template for your created MR.
2. Use this created branch for your changes.
3. After finishing your work remove the WIP status of your MR and assing the MR to Dustin Matzel.
